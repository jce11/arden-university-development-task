<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/*
Code to create table:
CREATE TABLE `enquires` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `first_name` varchar(60) DEFAULT NULL,
 `last_name` varchar(60) DEFAULT NULL,
 `email` varchar(100) DEFAULT NULL,
 `comment` varchar(300) DEFAULT NULL,
 `date_time` datetime DEFAULT NULL,
 PRIMARY KEY (`id`)
) 
Database is called Arden
Mysqli is used to connect to database and retrieve all records, these are stored in a assoc array.
this is looped through in the file and displayed using bootstrap aria controls in a list type format.
Font awesome is also used to get nice drop down icons.
Image has been compressed to increase page efficiency and is responsive to adapt to different screen sizes.

Sample inserts for database:
    INSERT INTO `enquires` (`id`, `first_name`, `last_name`, `email`, `comment`, `date_time`) VALUES (NULL, 'Luke', 'wells', 'Luke.Wells@gmail.com', 'This course looks amazing!', '2020-01-21 00:37:01');
INSERT INTO `enquires` (`id`, `first_name`, `last_name`, `email`, `comment`, `date_time`) VALUES (NULL, 'martin', 'jackson', 'martin.jackson@gmail.com', 'This course looks so good!', '2020-01-21 00:37:01');

*/

$mysqli = new mysqli("localhost", "root", "root", "Arden");

/* check connection */
if ($mysqli->connect_errno) {
    printf("Connect failed: %s\n", $mysqli->connect_error);
    exit();
}

$query = "SELECT * FROM enquires ORDER BY date_time ASC";
$enquires = [];

if ($result = $mysqli->query($query)) {

    /* fetch associative array */
    while ($row = $result->fetch_assoc()) {
        //printf ("%s (%s)\n", $row["id"], $row["last_name"]);
        $enquires[] = $row;
    }

    /* free result set */
    $result->free();
}
//var_dump($enquires);

/* close connection */
$mysqli->close();
 
?>

<!DOCTYPE html>
<html>
<head>
<title>Courses</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/e1b0a2eaf4.js" crossorigin="anonymous"></script>
</head>
<body>


<div class='header'>
  <div class='banner'>
  <img src="karen.png" alt="Banner" height="598" width ="1854" />
  </div>
</div>
<br>

<div class="container">
    <div class="card">
        <div class="card-body cards">
        <h1 class="card-title">Enquiries</h1>
        <?php
        
           foreach($enquires as $key => $students){
            ?>
            <div class="students">
                <div class="row ">
                    <div class="name">
                        <a data-toggle="collapse" class="enq" href="#col<?php echo $students['id']; ?>" aria-expanded="false" aria-controls="col"><?php echo $students['first_name'] . ' '  . $students['last_name']; ?></a><br>
                    </div>
                    <div class="drop_down">    
                        <i class="fas fa-chevron-down"></i>
                    </div>
                </div>
            <?php    
                echo '<div class="collapse" id="col'.$enquires[$key]['id'].'">';
                echo '<p class="card-text"> '. $enquires[$key]['comment'].'</p>';
                echo '<p class="card-text">Email: ' . $enquires[$key]['email'] . ' Date: ' . $enquires[$key]['date_time'] .'</p';
                echo '</div><br><br></div></div>';
            }
            ?>
            </div>
        </div>
    </div>
</div>
</body>
<style>
.name{
    float: left;
}

.drop_down{
    float: right;
    display: inline;
    
}
.students{
    background-color: white;
    background-width: 100%;
    margin: 20px;
}
.enq{
    margin: 10px;
    padding:8px;
    color: black;
}
.card-text{
    margin: 20px;
}
.banner img {
  width: 100%;
  height: 100%;
  display: block;
}
    .container{
        width: 80%;
        
    }
    .card{
        background-color: #e7e1e8;
    }

</style>
</html>